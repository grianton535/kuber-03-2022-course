variable "CLOUD_ID" {}
variable "AUTH_TOKEN" {}
variable "FOLDER_ID" {}
variable "SERVICE_ACCOUNT_ID" {}
variable "SSH_KEYS" {}


terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.AUTH_TOKEN
  cloud_id  = var.CLOUD_ID
  folder_id = var.FOLDER_ID
  zone      = "ru-central1-b"
}

resource "yandex_vpc_network" "k8s-net" { name = "k8s-net" }

resource "yandex_vpc_subnet" "k8s-subnet" {
 name = "k8s-subnet"
 v4_cidr_blocks = ["10.1.0.0/16"]
 zone           = "ru-central1-b"
 network_id     = yandex_vpc_network.k8s-net.id
}

resource "yandex_kubernetes_cluster" "test-k8s" {
 name = "test-k8s"
 network_id = yandex_vpc_network.k8s-net.id
 master {
   public_ip = true
   zonal {
     zone      = yandex_vpc_subnet.k8s-subnet.zone
     subnet_id = yandex_vpc_subnet.k8s-subnet.id
   }
 }
 service_account_id      = var.SERVICE_ACCOUNT_ID
 node_service_account_id = var.SERVICE_ACCOUNT_ID
}

resource "yandex_kubernetes_node_group" "test-node-group" {
  name = "test-node-group"
  cluster_id = yandex_kubernetes_cluster.test-k8s.id
  instance_template {
    platform_id = "standard-v2"

     metadata =  {
      ssh-keys = var.SSH_KEYS
     }

     network_interface {
      nat                = true
      subnet_ids         = [yandex_vpc_subnet.k8s-subnet.id]
    }
    
    resources {
      memory = 2
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }
  }
  scale_policy {
    fixed_scale {
      size = 1
    }
  }
}
